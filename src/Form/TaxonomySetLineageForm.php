<?php

namespace Drupal\taxonomy_set_lineage\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Implements the vocabulary selection form.
 */
class TaxonomySetLineageForm extends ConfigFormBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a TaxonomySetLineageVocabularyForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity bundle info.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_set_lineage_vocabulary_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['taxonomy_set_lineage.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['intro'] = [
      '#markup' => $this->t("Use the options below to set the scope for this module. You must select the vocabularies this module will be active for. You can then optionally limit it to specific entity types, bundles or fields."),
    ];

    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    foreach ($vocabularies as $vocabulary) {
      $vocabulary_options[$vocabulary->id()] = $vocabulary->label();
    }

    $configuration = $this->config('taxonomy_set_lineage.settings');

    $form['vocabulary'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Taxonomy Vocabularies'),
      '#description' => $this->t('The lineage will be saved for the selected vocabularies. If none of the options below are selected then this will happen for all entity types, bundles or fields.'),
      '#description_display' => 'before',
      '#options' => $vocabulary_options,
      '#default_value' => $configuration->get('vocabulary') ?: [],
      '#required' => TRUE,
    ];

    $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);

    $options_entities = [];
    $options_bundles = [];
    $options_fields = [];

    foreach ($fields as $field) {
      $instances = $this->entityTypeManager->getStorage('field_config')->loadByProperties([
        'field_name' => $field->get('field_name'),
      ]);

      foreach ($instances as $instance) {
        $entity_type = $instance->get('entity_type');
        $entity_label = $this->entityTypeManager->getDefinition($entity_type)->get('label');
        $options_entities[$entity_type] = $entity_label;

        $bundle_id = $instance->get('bundle');
        $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type)->get('bundle_entity_type');
        $entity_bundle = $this->entityTypeManager->getStorage($bundle_entity_type)->load($bundle_id);
        $options_bundles[$bundle_id] = $entity_label . ' - ' . $entity_bundle->label();

        $options_fields[$instance->id()] = $instance->label() . ' <small>(' . $instance->id() . ')</small>';
      }
    }

    $form['entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity Types'),
      '#description' => $this->t('The lineage will be saved for the active vocabularies for the selected entity types.'),
      '#description_display' => 'before',
      '#options' => $options_entities,
      '#default_value' => $configuration->get('entities') ?: [],
      '#required' => FALSE,
    ];
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity Bundles'),
      '#description' => $this->t('The lineage will be saved for the active vocabularies for the selected entity bundles. This does not require the entity above to be selected.'),
      '#description_display' => 'before',
      '#options' => $options_bundles,
      '#default_value' => $configuration->get('bundles') ?: [],
      '#required' => FALSE,
    ];
    $form['fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Taxonomy Reference Fields'),
      '#description' => $this->t('The linage will be saved for these specific fields only. This does not require you to select the corresponding entity or bundle.'),
      '#description_display' => 'before',
      '#options' => $options_fields,
      '#default_value' => $configuration->get('fields') ?: [],
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vocabulary = array_filter($form_state->getValue('vocabulary'));
    $entities = array_filter($form_state->getValue('entities'));
    $bundles = array_filter($form_state->getValue('bundles'));
    $fields = array_filter($form_state->getValue('fields'));

    $this->config('taxonomy_set_lineage.settings')
      ->set('vocabulary', array_values($vocabulary))
      ->set('entities', array_values($entities))
      ->set('bundles', array_values($bundles))
      ->set('fields', array_values($fields))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
