<?php

namespace Drupal\Tests\taxonomy_set_lineage\Functional;

/**
 * Functional Test for the Taxonomy Set Lineage module.
 *
 * @group taxonomy_set_lineage
 */
class TaxonomySetLineageContentTest extends TaxonomySetLineageTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'administer taxonomy',
      'administer nodes',
      'bypass node access',
      'access administration pages',
      'access content overview',
    ]));
  }

  /**
   * Check that the lineage is saved when .
   */
  public function testSave() {
    $parent_term = $this->createTerm($this->taxonomyVocabulary);
    $child_term = $this->createTerm($this->taxonomyVocabulary, ['parent' => $parent_term->id()]);
    $grandchild_term = $this->createTerm($this->taxonomyVocabulary, ['parent' => $child_term->id()]);

    // Post an article.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName();
    $edit['body[0][value]'] = $this->randomMachineName();
    $edit["{$this->taxonomyFieldName}[]"] = $child_term->id();
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');

    // Module is not configured yet - make sure it doesn't do anything!
    $first_node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $field_values = $first_node->get($this->taxonomyFieldName)->getValue();
    $this->assertCount(1, $field_values, 'Taxonomy field should only have one value.');
    $value = reset($field_values);
    $this->assertEquals($child_term->id(), $value['target_id'], 'Taxonomy term id does not match.');

    // Save configuration to enable module for our test vocabulary.
    $this->setConfig();

    // Post another article.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName();
    $edit['body[0][value]'] = $this->randomMachineName();
    $edit["{$this->taxonomyFieldName}[]"] = $grandchild_term->id();
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');

    $second_node = $this->drupalGetNodeByTitle($edit['title[0][value]']);

    // Check that the parent and grandparent terms have been added.
    $field_values = $second_node->get($this->taxonomyFieldName)->getValue();
    $this->assertCount(3, $field_values, 'Taxonomy field should now have three values.');

    foreach ($field_values as $delta => $value) {
      if ($delta == 0) {
        $this->assertEquals($parent_term->id(), $value['target_id'], 'First item is not parent term.');
      }
      elseif ($delta == 1) {
        $this->assertEquals($child_term->id(), $value['target_id'], 'Second item is not child term.');
      }
      elseif ($delta == 2) {
        $this->assertEquals($grandchild_term->id(), $value['target_id'], 'Third item is not grandchild term.');
      }
    }

    // Test the Action on the content administration page using first node.
    $this->drupalGet('/admin/content');
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('action', 'taxonomy_set_lineage_update_node');
    // Need to generate the bulk form key for our node.
    $key_parts = [$first_node->language()->getId(), $first_node->id()];
    $key = json_encode($key_parts);
    $bulk_key = base64_encode($key);

    $elements = $this->cssSelect('input[value="' . $bulk_key . '"]');
    $checkbox = reset($elements);
    $checkbox->check();
    $page->pressButton('Apply to selected items');

    // After operation, load node page and check content for both terms.
    $this->drupalGet('/node/' . $first_node->id());
    $this->assertSession()->pageTextContains($parent_term->label());
    $this->assertSession()->pageTextContains($child_term->label());
  }

}
